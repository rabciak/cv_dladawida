### Curriculum vitae wersja modyfikowana na porzeby zad2.
##### Dane osobowe
 Imię i nazwisko: **Tomasz Ratyna**
 Adres e-mail: **rabciak@gmail.com**
 Numer telefonu: **500000000**
##### Wykształcenie:
- Technik informatyk
- Licencjat z Geoinformatyki
##### Doświadczenie:
- 2016: **Vietnam house**
    - Kierowca
- 2018:  **MTS GIS**
    - Opracowywanie map glebowo roliczych dla **[ARiMR](https://www.arimr.gov.pl/)**
    - Weryfikacja danych w bazie **[BDOT10k](https://www.geoportal.gov.pl/dane/baza-danych-obiektow-topograficznych-bdot)**
- 2019 **[xploria](www.xploria.io) / [veo](veo.glass)**
    - Prace wektoryzacyjne dla projektu okularów AR dla żeglarzy 
##### Umiejętności:
- Doskonała znajomość oprogramowania GIS _(QGis, Arcmap, Geomedia itp..)_
- Podstawowa znajomość języka Python _(Głównie na platformie QGis)_
- Znajomość baz danych SQL _(PostgreSQL)_
- Umiejętność pracy w zespole oraz komunikatywność
##### Zainteresowania:
Motoryzacja, gry komputerowe, poker, wędkarstwo, penspinning, tenis stołowy, nowe technologie.
